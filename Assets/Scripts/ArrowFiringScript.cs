﻿using UnityEngine;
using System.Collections;

public class ArrowFiringScript : MonoBehaviour {

    public bool axisX = false;
    public bool axisY = false;
    public bool axisZ = true;
    public Transform firePoint;
    bool isOn = true;
    AudioSource aud;

    public AudioClip arrowSound;

    GameObject arrow;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (isOn)
        {
            isOn = false;
            StartCoroutine("TimedShooting");
        }


      



    }

   
    IEnumerator TimedShooting()
    {

        if (axisX)
        {
            aud = GetComponent<AudioSource>();
            aud.PlayOneShot(arrowSound, 0.4f);

            arrow = (GameObject)Instantiate(Resources.Load("Arrow"), firePoint.position, Quaternion.Euler(90, 90, 0));
            arrow.GetComponent<Rigidbody>().AddForce(new Vector3(-20, 0,0), ForceMode.Impulse);
            yield return new WaitForSeconds(1.5f);
            isOn = true;
        }
        if (axisY)
        {
            arrow = (GameObject)Instantiate(Resources.Load("Arrow"), firePoint.position, Quaternion.Euler(90, 0, 0));
            arrow.GetComponent<Rigidbody>().AddForce(new Vector3(0, 20, 0), ForceMode.Impulse);
            yield return new WaitForSeconds(1.5f);
            isOn = true;
        }
        if (axisZ)
        {
            arrow = (GameObject)Instantiate(Resources.Load("Arrow"), firePoint.position, Quaternion.Euler(90, 0, 0));
            arrow.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -20), ForceMode.Impulse);
            yield return new WaitForSeconds(1.5f);
            isOn = true;
        }

    }
   

}
