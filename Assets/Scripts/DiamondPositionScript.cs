﻿using UnityEngine;
using System.Collections;

public class DiamondPositionScript : MonoBehaviour {

    Vector3[] positions = { new Vector3 { x = 157.86f, y = 0.5f, z = 37.64f },
                         new Vector3 { x = 72.1f, y = 0.5f, z = 51.7f},
        new Vector3 { x = 57.6f  , y = 0.5f, z = 75.2f},
        new Vector3 { x = 62.3f, y = 0.5f, z = 128.7f},
        new Vector3 { x = 140.2f , y = 0.5f, z = 147.1f}, };


    // Use this for initialization
    void Start () {

        Instantiate(Resources.Load("DiamondWhite"), positions[ Random.Range(0, 5)], Quaternion.identity);
        Instantiate(Resources.Load("DiamondPurple"), positions[Random.Range(1, 5)], Quaternion.identity);
        Instantiate(Resources.Load("DiamondRed"), positions[Random.Range(2, 5)], Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
