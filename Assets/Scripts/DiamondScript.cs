﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DiamondScript : MonoBehaviour {
    public Image redGem;
    public Image purpleGem;
    public Image sliverGem;
    int gemsCollected = 0;
    public GameObject gameEnd;
    public GameObject spotLight;
    public GameObject gameCompletePanel;
   
    public GameObject gemCollector;

    // Use this for initialization
    void Start () {
        redGem.enabled = false;
        purpleGem.enabled = false;
        sliverGem.enabled = false;
        gameEnd.SetActive(false);
        spotLight.GetComponent<Light>().enabled = false;
        gemCollector.GetComponent<BoxCollider>().enabled = false;
        gameCompletePanel.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {

        if (gemsCollected == 3)
        {
            gameEnd.SetActive(true);
            spotLight.GetComponent<Light>().enabled = true;
            gemCollector.GetComponent<BoxCollider>().enabled = true;

        }



    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "redgem")
        {
            Destroy(collision.gameObject);
            redGem.enabled = true;
            gemsCollected++;


        }

        if(collision.gameObject.tag == "gemCollector")
        {
            Instantiate(Resources.Load("DiamondBlue"), new Vector3(85.64f, 0.5260364f, 107.8671f), Quaternion.identity);
            Instantiate(Resources.Load("DiamondGreen"), new Vector3(85.5f, 0.5260364f, 108.9841f), Quaternion.identity);
            Instantiate(Resources.Load("DiamondOrange"), new Vector3(85.4f, 0.5260364f, 110.1543f), Quaternion.identity);
            Instantiate(Resources.Load("DiamondPurple"), new Vector3(85.4f, 0.5260364f, 110.1543f), Quaternion.identity);
            Instantiate(Resources.Load("DiamondRed"), new Vector3(85.08f, 0.5260364f, 112.37f), Quaternion.identity);
            Instantiate(Resources.Load("DiamondWhite"), new Vector3(84.66f, 0.5260364f, 113.55f), Quaternion.identity);
            Instantiate(Resources.Load("treasurechest"), new Vector3(84.19089f, 0.21f, 115.9748f), Quaternion.identity);
            StartCoroutine("gameComplete");
        }


        if (collision.gameObject.tag == "purplegem")
        {
            Destroy(collision.gameObject);

            purpleGem.enabled = true;

            gemsCollected++;
        }
        if (collision.gameObject.tag == "slivergem")
        {
            Destroy(collision.gameObject);
            gemsCollected++;
            sliverGem.enabled = true;
        }
    }

   IEnumerator gameComplete()
    {
        yield return new WaitForSeconds(2f);

        gameCompletePanel.SetActive(true);
    }

}
