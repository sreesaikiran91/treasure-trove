﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void playGame()
    {
        Application.LoadLevel("Game Level One");
    }
    public void gameInstructions()
    {
        Application.LoadLevel("Instructions");

    }
    public void gameControls()
    {
        Application.LoadLevel("Controls");

    }
    public void quitGame()
    {
        Application.Quit();
    }
}
