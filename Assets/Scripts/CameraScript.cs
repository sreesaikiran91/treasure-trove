﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        
       }

    void OnTriggerEnter(Collider collision)
    {

       
            collision.gameObject.GetComponent<MeshRenderer>().enabled = false;
        //  collision.gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;
        Renderer[] rs = collision.gameObject.GetComponentsInChildren<MeshRenderer>();
            foreach (Renderer r in rs)
            r.enabled = false;
    }

    void OnTriggerExit(Collider collision)
    {
       
            collision.gameObject.GetComponent<MeshRenderer>().enabled = true;
        Renderer[] rs = collision.gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach (Renderer r in rs)
            r.enabled = true;

    }

}
