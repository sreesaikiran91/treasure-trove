﻿using UnityEngine;
using System.Collections;

public class SpikeMovementScript : MonoBehaviour {

    public bool axisX = true;
    public bool axisY = false;
    public bool axisZ = false;


    float floatStrength = 2;
    float originalX;
    float originalY;
    float originalZ;

    // Use this for initialization
    void Start () {
        

        originalX = this.transform.position.x;
        originalY = this.transform.position.y;
        originalZ = this.transform.position.z;


    }

    // Update is called once per frame
    void Update () {

        if (axisX) {
            transform.position = new Vector3(originalX + ((float)System.Math.Sin(Time.time) * floatStrength), transform.position.y, transform.position.z);
        }
        else if(axisY)
        {
            transform.position = new Vector3(transform.position.x, originalY + ((float)System.Math.Sin(Time.time) * floatStrength), transform.position.z);

        }
        else if (axisZ)
        {
            transform.position = new Vector3(transform.position.x,transform.position.y , originalZ + ((float)System.Math.Sin(Time.time) * floatStrength));

        }
    }

    



}
