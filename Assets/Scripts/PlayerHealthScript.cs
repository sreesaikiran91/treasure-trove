﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;


public class PlayerHealthScript : MonoBehaviour {

    public GameObject GameoverCanvas;
    public int startingHealth = 100;                            // The amount of health the player starts the game with.
    public int currentHealth;                                   // The current health the player has.
    public Slider healthSlider;                                 // Reference to the UI's health bar.
    public Image damageImage;                                   // Reference to an image to flash on the screen on being hurt.
   // public AudioClip deathClip;                                 // The audio clip to play when the player dies.
    public float flashSpeed = 5f;                               // The speed the damageImage will fade at.
    public Color flashColour = new Color(1f, 0f, 0f, 190f);     // The colour the damageImage is set to, to flash.
    UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl control;
    UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter character;
    public Text Healthpercent;
    public Transform SpawnPoint;
  bool isDead;                                                // Whether the player is dead.
    bool damaged;                                               // True when the player gets damaged.
    public int playerLife = 3 ;
    public Text PlayerLifeText;
    void Awake()
    {
        GameoverCanvas.SetActive(false);

        PlayerLifeText.text = playerLife.ToString();
        // Set the initial health of the player.
        currentHealth = startingHealth;
        Healthpercent.text = currentHealth.ToString()+"%";
    }
   

    void Update()
    {

        
        PlayerLifeText.text = playerLife.ToString();

       

        Healthpercent.text = currentHealth.ToString()+"%";

        // If the player has just been damaged...
        if (damaged)
        {
            // ... set the colour of the damageImage to the flash colour.
            damageImage.color = flashColour;
        }
        // Otherwise...
        else
        {
            // ... transition the colour back to clear.
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        // Reset the damaged flag.
        damaged = false;
    }


    public void TakeDamage(int amount)
    {
        // Set the damaged flag so the screen will flash.
        damaged = true;

        // Reduce the current health by the damage amount.
        currentHealth -= amount;

        // Set the health bar's value to the current health.
        healthSlider.value = currentHealth;

        // Play the hurt sound effect.
       // playerAudio.Play();

        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (currentHealth <= 0)
        {
            
            playerLife--;

            this.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            control = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>();
            control.enabled = false;
            character = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();
            character.enabled = false;

            this.GetComponent<Transform>().position = SpawnPoint.position;
            this.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
            control = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>();
            control.enabled = true;
            character = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();
            character.enabled = true;
            currentHealth = 100;
            startingHealth = 100;
            healthSlider.value = 100;

            // ... it should die.
            Death();
        }
    }


    void Death()
    {

        if (playerLife < 0)
        {
            isDead = true;
            this.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
            control = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>();
            control.enabled = false;
            character = GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>();
            character.enabled = false;
            GameoverCanvas.SetActive(true);

        }
       
        // Set the death flag so this function won't be called again.


    }


    void OnParticleCollision(GameObject collision)
    {
        if(collision.gameObject.tag == "flamethrow")
        {           
            TakeDamage(2);
            
        }

        if (collision.gameObject.tag == "meteor")
        {
           
            TakeDamage(2);

        }


    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "terrain")
        {

            TakeDamage(100);
        }

        if (collision.gameObject.tag == "axe")
        {

            TakeDamage(1);
        }


        if (collision.gameObject.tag == "groundspike")
        {
            
            TakeDamage(5);

        }
        if (collision.gameObject.tag == "sword")
        {

            TakeDamage(5);

        }
        if (collision.gameObject.tag == "mace")
        {

            TakeDamage(5);

        }
        if (collision.gameObject.tag == "groundsword")
        {

            TakeDamage(100);

        }
        if (collision.gameObject.tag == "Arrow")
        {

            TakeDamage(2);

        }
        if (collision.gameObject.tag == "HealthHeart")
        {

            Destroy(collision.gameObject);
            currentHealth = 100;
            healthSlider.value = 100;

        }
    }

    IEnumerator TimedSpawn()
    {

        
        yield return new WaitForSeconds(2f);
      
       
    }
}
