﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GemCountScript : MonoBehaviour {

    int gemsCollected = 0;
    // Use this for initialization
    public Image blueGem;
    public Image greenGem;
    public Image orangeGem;

    void Start () {

        blueGem.enabled = false;
        greenGem.enabled = false;
        orangeGem.enabled = false;


	
	}
	
	// Update is called once per frame
	void Update () {

        if (gemsCollected == 3)
        {
            Application.LoadLevel("Level two");
        }

	
	}


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bluegem")
        {
            Destroy(collision.gameObject);
            blueGem.enabled = true;
            gemsCollected++;
            

        }
        if (collision.gameObject.tag == "greengem")
        {
            Destroy(collision.gameObject);
           
            greenGem.enabled = true;

            gemsCollected++;
        }
        if (collision.gameObject.tag == "orangegem")
        {
            Destroy(collision.gameObject);
            gemsCollected++;
            orangeGem.enabled = true;
        }
    }
    }
