﻿using UnityEngine;
using System.Collections;

public class ControlScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void playAgain()
    {
        Application.LoadLevel("Level two");
    }

    public void mainMenu()
    {
        Application.LoadLevel("Game Start");
    }

    public void levelOnePlayAgain()
    {
        Application.LoadLevel("Game Level One");
    }
}
