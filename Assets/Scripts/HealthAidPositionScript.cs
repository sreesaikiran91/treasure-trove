﻿using UnityEngine;
using System.Collections;

public class HealthAidPositionScript : MonoBehaviour {

    Vector3[] positions = { new Vector3 { x = 62.58453f   , y = 2.95f, z = 50.73f },
                         new Vector3 { x = 28.9f, y = 2.95f, z = 50.5f},
        new Vector3 { x = 33.9f, y = 2.95f, z = 26.6f},
        new Vector3 { x = 49.7f, y = 2.95f, z = 91f },
        new Vector3 { x = 65.86f, y = 2.95f, z = 68.31f} };


    void Start()
    {

        for (int i = 0; i < positions.Length; i++) {
            Instantiate(Resources.Load("Heart"), positions[i], Quaternion.identity);
        }

    }


    // Update is called once per frame
    void Update () {
	
	}
}
