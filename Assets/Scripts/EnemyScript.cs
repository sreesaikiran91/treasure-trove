﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
  public  Transform player;               // Reference to the player's position.
   public  NavMeshAgent nav;               // Reference to the nav mesh agent.


    void Awake()
    {
        // Set up the references.
       // player = GameObject.FindGameObjectWithTag("Player").transform;
        nav = GetComponent<NavMeshAgent>();
    }


    void Update()
    {
        
            // ... set the destination of the nav mesh agent to the player.
            
        var distance = Vector3.Distance(player.position, this.GetComponent<Transform>().position);

        if (distance < 2.0f)
        {
            this.GetComponent<Animation>().CrossFade("Lumbering");
        }
        else {
            nav.SetDestination(player.position);
            this.GetComponent<Animation>().CrossFade("Walk");
        }


    }
}
