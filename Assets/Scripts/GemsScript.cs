﻿using UnityEngine;
using System.Collections;

public class GemsScript : MonoBehaviour {

    float rotSpeed = 60;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<Transform>().Rotate(0, rotSpeed * Time.deltaTime, 0, Space.World);
    }
}
