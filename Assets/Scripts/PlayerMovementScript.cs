﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour {
    public float speed = 6f;            // The speed that the player will move at.

    Vector3 movement;                   // The vector to store the direction of the player's movement.
   
    Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    public float RotateSpeed = 3f;
    // Use this for initialization
    void Start () {
      
        playerRigidbody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        
}

    void FixedUpdate()
    {
        // Store the input axes.
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // Move the player around the scene.
        Move(h, v);


        
    }

    void Move(float h, float v)
    {
        // Set the movement vector based on the axis input.
        movement.Set(h, 0f, v);

        // Normalise the movement vector and make it proportional to the speed per second.
        movement = movement.normalized * speed * Time.deltaTime;

        // Move the player to it's current position plus the movement.
        playerRigidbody.MovePosition(transform.position + movement);


        if (Input.GetKey(KeyCode.LeftArrow))
            this.GetComponent<Transform>().Rotate(-Vector3.up * RotateSpeed * Time.deltaTime);
        else if (Input.GetKey(KeyCode.RightArrow))
            this.GetComponent<Transform>().Rotate(Vector3.up * RotateSpeed * Time.deltaTime);
    



        if (Input.GetKey(KeyCode.UpArrow))
        {

            this.GetComponent<Animation>().CrossFade("Run");
        }

        else {

            this.GetComponent<Animation>().CrossFade("idle");
        }

        if (Input.GetKey(KeyCode.Space))
        {

            this.GetComponent<Animation>().CrossFade("Jump");
        }
        if (Input.GetKey(KeyCode.B))
        {

            this.GetComponent<Animation>().CrossFade("Attack");
        }



    }
    }
    


