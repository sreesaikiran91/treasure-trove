﻿using UnityEngine;
using System.Collections;

public class GemPositionScript : MonoBehaviour {

    // Use this for initialization

    Vector3[] positions = { new Vector3 { x = 16.4f, y = 2.466f, z = 48.6f },
                         new Vector3 { x = 77.8f, y = 2.466f, z = 55.1f},
        new Vector3 { x = 66.76f, y = 2.466f, z = 69.6f},
        new Vector3 { x = 30.2f, y = 2.466f, z = 70.2f},
        new Vector3 { x = 67.1f, y = 2.466f, z = 89.5f},
        new Vector3 { x = 31.45f, y = 2.466f, z = 92.1f}, };


    void Start () {


        Instantiate(Resources.Load("DiamondBlue"), positions[Random.Range(0,6)], Quaternion.identity);
        Instantiate(Resources.Load("DiamondGreen"), positions[Random.Range(1, 6)], Quaternion.identity);
        Instantiate(Resources.Load("DiamondOrange"), positions[Random.Range(2, 6)], Quaternion.identity);

    }

    // Update is called once per frame
    void Update () {
       
	}
}
