﻿using UnityEngine;
using System.Collections;

public class FirePositionScript : MonoBehaviour {
    bool isOn = true;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            isOn = false;

            StartCoroutine("createFlame");

        }
    }

    IEnumerator createFlame()
    {

        Instantiate(Resources.Load("Flamethrower"), new Vector3(157.26f, 1.69f, 161.59f), Quaternion.Euler(new Vector3(0, -90, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(35.5f, 1.7f, 154.1f), Quaternion.Euler(new Vector3(0, 10, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(25.46f, 1.2f, 39.54f), Quaternion.Euler(new Vector3(0, -30, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(176.16f, 0.46f, 57.8f), Quaternion.Euler(new Vector3(0, -70, 0)));

        yield return new WaitForSeconds(4);
        isOn = true;


    }


}
