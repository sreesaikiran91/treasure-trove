﻿using UnityEngine;
using System.Collections;

public class FireControllerScript : MonoBehaviour {
    bool isOn = true;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (isOn)
        {
            isOn = false;

            StartCoroutine("createFlame");
          
        }   
    }

    IEnumerator createFlame()
    {

        Instantiate(Resources.Load("Flamethrower"), new Vector3(44.19f, 3.29f, 31.82244f), Quaternion.Euler(new Vector3(0,90,0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(36.49625f, 3.330931f, 66.20673f), Quaternion.Euler(new Vector3(0, 0, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(63.43f, 3.13f, 26.88f), Quaternion.Euler(new Vector3(0, 180, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(26.95f, 3.02f, 46.81f), Quaternion.Euler(new Vector3(0, 0, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(63.02f, 3.063477f, 73.06223f), Quaternion.Euler(new Vector3(0, 180, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(73.79f, 3.29f, 44.35f), Quaternion.Euler(new Vector3(0, 0, 0)));
        Instantiate(Resources.Load("Flamethrower"), new Vector3(64.1f, 3.12f, 87.1f), Quaternion.Euler(new Vector3(0, 0, 0)));

        yield return new WaitForSeconds(3);
        isOn = true;


    }
   

}
