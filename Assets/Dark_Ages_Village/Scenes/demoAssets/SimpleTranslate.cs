﻿using UnityEngine;
using System.Collections;

public class SimpleTranslate : MonoBehaviour
{

	public Vector3 move = Vector3.zero;
	public Vector3 rotate = Vector3.zero;



	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{

		transform.Translate(move.x * Time.deltaTime, move.y * Time.deltaTime, move.z * Time.deltaTime);
		transform.Rotate(rotate.x * Time.deltaTime, rotate.y * Time.deltaTime, rotate.z * Time.deltaTime);

	}

}
